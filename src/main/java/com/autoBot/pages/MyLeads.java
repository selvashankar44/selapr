package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyLeads extends Annotations {
		
		public Crealea clickcreateLeadsbutton() {
			WebElement Clead = locateElement("link", "Create Lead");
			click(Clead);  
			
			return new Crealea();
		}

}
