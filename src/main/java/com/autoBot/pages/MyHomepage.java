package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class MyHomepage extends Annotations {
	
	public MyLeads clickLeadsbutton() {
		WebElement lea = locateElement("link", "Leads");
		click(lea);  
		
		return new MyLeads();
	}

}
