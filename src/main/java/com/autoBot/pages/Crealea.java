package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class Crealea extends Annotations {
	
	public Crealea entercompname(String data) {

		WebElement compname = locateElement("id", "createLeadForm_companyName");
		clearAndType(compname, data);  
		return this; 

	}
	
	public Crealea enterfirstname(String data) {

		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstname, data);  
		return this; 

	}
	
	public Crealea enterlastname(String data) {

		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastname, data);  
		return this; 

	}
	
	public Createleadsub clicksubmit() {
		
			WebElement sub = locateElement("name", "submitButton");
			click(sub);  
			
			return new Createleadsub();
	}
	
	

}
